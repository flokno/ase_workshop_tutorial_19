import numpy as np


def inscribed_sphere_in_box(cell):
    """Find the radius of an inscribed sphere in a unit cell

    Args:
        cell (np.ndarray): Cell where the sphere should be inscribed

    Returns:
        float: The radius of the inscribed sphere
    """

    # the normals of the faces of the box
    na = np.cross(cell[1, :], cell[2, :])
    nb = np.cross(cell[2, :], cell[0, :])
    nc = np.cross(cell[0, :], cell[1, :])
    na /= np.linalg.norm(na)
    nb /= np.linalg.norm(nb)
    nc /= np.linalg.norm(nc)
    # distances between opposing planes
    rr = 1.0e10
    rr = min(rr, abs(na @ cell[0, :]))
    rr = min(rr, abs(nb @ cell[1, :]))
    rr = min(rr, abs(nc @ cell[2, :]))
    rr *= 0.5
    return rr

